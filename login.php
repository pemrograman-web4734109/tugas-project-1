<?php
    session_start();
    if(isset($_SESSION['uid'])){
        header('location:dashboard/dashboard.php');
    }
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name = "viewport" content="width=device-width, initial-scale=1">
        <title>Login - Food Recipe</title>
        <style type="text/css">
            @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans;wght@300;400;700&display=swap');
            * {
                padding: 0;
                margin: 0;
            }
            body {
                font-family: 'Nunito Sans', sans-serif;
                background-color: #F9F1F0;
            }
            a {
                color: inherit;
                text-decoration: none;
            }
            .container {
                width: 100%;
                height: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .card-login {
                border: 1px solid;
                background-color: #fff;
                width: 300px;
                padding: 25px 15px;
                box-sizing: border-box;
                border-radius: 5px;
            }
            .card-login h2 {
                margin-bottom: 10px;
                text-align: center;
            }
            .input-control {
                width: 100%;
                display: block;
                padding: 0.5rem 1rem;
                box-sizing: border-box;
                font-size: 1rem;
                margin-bottom: 8px;
            }
            .btn {
                display: block;
                width: 100%;
                padding: 0.5rem 1rem;
                cursor: pointer;
                font-size: 1rem;
                background-color: #8ca16d;
                color: black;
                margin-top: 8px;
                border-radius: 5px;
                border-color: #627254;
                outline-color: #8ca16d;
            }
            .btn:hover {
                background-color: #627254;
                color: #fff;
            }
            
        </style>
    </head>
    <body>
        <!-- login -->
        <div class="container">
            <div class="card-login">

                <h2>Login</h2>
                <form action="" method="post">
                    <input type="text" name="user" placeholder="Username" class="input-control">
                    <input type="password" name="pass" placeholder="Password" class="input-control">
                    <button type="submit" name="login" class="btn">Login</button>
                </form>

                <?php
                    //cek jika tombol login ditekan
                    if(isset($_POST['login'])){

                        include 'database.php';

                        //cek data login
                        $query_select = 'select * from user
                        where username = "'.$_POST['user'].'"
                        and password = "'.$_POST['pass'].'" ';

                        $run_query_select = mysqli_query($conn, $query_select);
                        $d = mysqli_fetch_object($run_query_select);

                        if($d){
                            //buat session
                            $_SESSION['uid'] = $d->id_user;
                            $_SESSION['uname'] = $d->nama;

                            header('location:dashboard/dashboard.php');

                        } else{
                            echo 'Username atau password anda salah!';
                        }
                    }
                ?>

            </div>
        </div>

    </body>
</html>