-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2024 at 05:22 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resep`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabelresep`
--

CREATE TABLE `tabelresep` (
  `id_resep` int(11) NOT NULL,
  `nama_resep` varchar(100) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `alat` varchar(255) NOT NULL,
  `bahan` varchar(500) NOT NULL,
  `step` varchar(500) NOT NULL,
  `kategori` enum('Kue','Camilan','Makanan Berat','Minuman') NOT NULL,
  `foto` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tabelresep`
--

INSERT INTO `tabelresep` (`id_resep`, `nama_resep`, `deskripsi`, `alat`, `bahan`, `step`, `kategori`, `foto`, `time`) VALUES
(1, 'Nasi Goreng Jawa Spesial ', 'Nasi goreng yang lezat dengan bumbu spesial\r\nSiapa bilang nasi goreng harus biasa-biasa saja? Nasi goreng kami menghadirkan sensasi lezat yang bikin lidah bergoyang! Dibuat dengan cinta dan bumbu-bumbu pilihan, setiap suapan adalah perjalanan rasanya yang', 'Wajan, Sendok', '300g nasi putih, 100g daging ayam/sapi/udang, 2 butir telur, 1 buah bawang merah, 2 siung bawang putih, 1 batang daun bawang, 2 sdm kecap manis, 1 sdm saus tiram, 1 sdm minyak wijen', '1. Tumis bawang merah dan bawang putih hingga harum. 2. Masukkan daging, telur, dan nasi, aduk rata. 3. Tambahkan kecap manis, saus tiram, dan minyak wijen, aduk hingga merata. 4. Taburkan daun bawang, aduk sebentar, lalu angkat.\', \'makanan berat', 'Makanan Berat', 'nasi_goreng.jpg', '2024-04-15 03:19:11'),
(2, 'Pisang Goreng Crispy', 'Pisang goreng yang renyah diluar dan manis di dalam', 'Wajan, Penggorengan', '5 buah pisang kepok, 100g tepung terigu, 50g tepung beras, 2 sdm tepung maizena, 1 sdt baking powder, 1/2 sdt garam, 150ml air, Minyak untuk menggoreng', '1. Campurkan tepung terigu, tepung beras, tepung maizena, baking powder, dan garam. 2. Tambahkan air, aduk hingga tercampur rata. 3. Kupas pisang, potong sesuai selera. 4. Celupkan pisang ke dalam adonan tepung. 5. Goreng pisang hingga kecokelatan. 6. Angkat dan tiriskan.', 'Camilan', 'pisang_goreng.jpg', '2024-04-15 03:19:11'),
(3, 'Sop Ayam', 'Sop ayam hangat yang menyegarkan', 'Panci, Sendok', '500g daging ayam, 2 buah wortel, 2 buah kentang, 1 batang seledri, 1 batang daun bawang, 1 liter air, Garam secukupnya, Merica secukupnya', '1. Rebus air hingga mendidih. 2. Masukkan daging ayam, rebus hingga matang. 3. Tambahkan wortel dan kentang, masak hingga lunak. 4. Bumbui dengan garam dan merica. 5. Taburkan seledri dan daun bawang saat akan disajikan.', 'Makanan Berat', 'sop_ayam.jpg', '2024-04-15 03:19:11'),
(4, 'Pancake Coklat', 'Pancake lembut dengan rasa coklat yang kaya', 'Wajan, Sendok', '200g tepung terigu, 2 sdm cokelat bubuk, 2 sdm gula pasir, 1 butir telur, 200ml susu cair, 2 sdm mentega leleh', '1. Campurkan tepung terigu, cokelat bubuk, dan gula pasir. 2. Tambahkan telur dan susu, aduk hingga rata. 3. Panaskan wajan, tuangkan adonan pancake, goreng hingga matang. 4. Sajikan dengan mentega leleh.', 'Kue', 'pancake_coklat.jpg', '2024-04-15 03:19:11'),
(5, 'Martabak Telur', 'Martabak dengan isi telur dan daging yang gurih', 'Wajan, Penggorengan', '200g tepung terigu, 2 butir telur, 150ml air, 100g daging sapi/ayam cincang, 2 batang daun bawang, Garam secukupnya, Merica secukupnya', '1. Campurkan tepung terigu, telur, dan air, aduk hingga rata. 2. Tambahkan daging, daun bawang, garam, dan merica, aduk kembali. 3. Panaskan wajan, tuangkan adonan, ratakan. 4. Masak hingga matang di kedua sisi. 5. Lipat martabak dan sajikan.', 'Makanan Berat', 'martabak_telur.jpg', '2024-04-15 03:19:11'),
(6, 'Kue Brownies', 'Brownies dengan tekstur lembut dan rasa coklat yang khas', 'Loyang, Mixer', '200g tepung terigu, 200g gula pasir, 100g cokelat bubuk, 150g mentega, 3 butir telur, 1 sdt vanili bubuk, 1/2 sdt baking powder', '1. Campurkan tepung terigu, gula pasir, dan cokelat bubuk. 2. Lelehkan mentega, lalu campurkan dengan telur dan vanili bubuk. 3. Campurkan semua bahan dan aduk hingga rata. 4. Tuangkan adonan ke dalam loyang, ratakan. 5. Panggang dalam oven selama 30-40 menit. 6. Dinginkan sebelum dipotong dan disajikan.', 'Kue', 'brownies.jpg', '2024-04-15 03:19:11'),
(7, 'Es Buah Segar', 'Minuman segar dengan campuran buah-buahan', 'Gelas, Pisau', 'Jeruk, semangka, melon, apel, anggur secukupnya, Es batu secukupnya, Sirup secukupnya', '1. Potong semua buah menjadi dadu kecil. 2. Campur semua buah dalam gelas. 3. Tambahkan es batu dan sirup sesuai selera. 4. Aduk rata sebelum disajikan.', 'Minuman', 'es_buah.jpg', '2024-04-15 03:19:11'),
(8, 'Roti Bakar Keju', 'Roti yang dipanggang dengan taburan keju yang meleleh', 'Oven, Sendok', '4 potong roti tawar, Mentega secukupnya, Keju parut secukupnya', '1. Olesi roti dengan mentega. 2. Taburi keju parut di atas roti. 3. Panggang dalam oven hingga keju meleleh dan roti kecokelatan. 4. Angkat dan sajikan.', 'Camilan', 'roti_bakar_keju.jpg', '2024-04-15 03:19:11'),
(9, 'Mie Goreng Sederhana', 'Mie yang digoreng dengan bumbu sederhana', 'Wajan, Sendok', '2 bungkus mie instan, 2 butir telur, 1 buah wortel, 2 batang daun bawang, 2 sdm kecap manis, 1 sdm saus tiram, Garam secukupnya, Minyak untuk menumis', '1. Rebus mie hingga matang, tiriskan. 2. Panaskan minyak, tumis bawang putih dan telur. 3. Masukkan wortel, daun bawang, dan mie, aduk rata. 4. Tambahkan kecap manis, saus tiram, dan garam, aduk hingga tercampur merata. 5. Angkat dan sajikan.', 'Makanan Berat', 'mie_goreng.jpg', '2024-04-15 03:19:11'),
(10, 'Es Teh Manis', 'Minuman teh dingin dengan rasa manis', 'Gelas, Sendok', '2 kantong teh, Gula secukupnya, Es batu secukupnya', '1. Rebus air hingga mendidih. 2. Masukkan kantong teh, diamkan beberapa menit. 3. Tuangkan teh ke dalam gelas. 4. Tambahkan gula secukupnya, aduk hingga larut. 5. Tambahkan es batu dan sajikan.', 'Minuman', 'es_teh.jpg', '2024-04-15 03:19:11'),
(11, 'Tanghulu', 'Tanghulu adalah manisan dengan bahan dasar buah yang berasal dari Cina. Biasanya tanghulu sering disajikan di saat musim dingin tiba. Manisan Buah ini mudah sekali ditemukan di toko-toko makanan, maupun pedagang kaki lima. Camilan yang punya rasa manis in', 'panci, sendok, tusuk sate.', '6 buah stroberi ukuran sedang\r\n100 ml gula pasir\r\n70 ml air\r\n2 sdm glucose/corn syrup\r\nTusuk sate', '1. Persiapkan bahan. Untuk glucose bisa dibeli di toko bahan kue yaa, ini fungsinya agar gula dapat mengeras seperti permen.\r\n2. Bersihkan stroberi, lalu tusuk dengan tusuk sate.\r\n3. Rebus gula, glucose, dan air dengan api sedang hingga meletup-letup, hati-hati gosong. Cara mengecek bahwa larutsan gula sudah siap adalah dengan meneteskan larutan gula ke air, jika gula tsb langsung mengeras dan berbunyi seperti retakan kaca artinya gula sudah siap.\r\n4. Matikan api. Lalu dengan cepat siram strober', 'Camilan', 'tanghulu-recipe-IG.jpg', '2024-04-15 10:29:13'),
(12, 'Kepiting Saos Padang', 'Dagingnya yang lembut dan manis alami akan semakin lezat dicocolkan ke dalam saus pedas dan gurih ala Padang ini.\r\n\r\n', '1. Wajan\r\n2. Pisau\r\n3. Penggorengan', '1. Kepiting - 4 ekor\r\n2. Bawang bombai besar, iris panjang - 1 buah\r\n3. Cabai merah besar, iris serong - 2 buah\r\n4. Saus sambal - 6 sdm\r\n5. Saus tomat - 3 sdm\r\n6. Kecap manis - 1 sdm\r\n7. Kecap inggris - 1 sdm\r\n8. Lada bubuk - 1/2 sdt\r\n9. Garam - 1/2 sdt\r\n10. Gula pasir - 1 sdt\r\n11. Daun bawang, iris - 1 batang\r\n12. Air - 200 ml\r\n13. Mentega - 2 sdm\r\n\r\nBUMBU HALUS:\r\n1. Bawang putih - 6 siung\r\n2. Bawang merah - 2 butir\r\n3. Cabai merah besar, buang isinya - 4 buah\r\n4. Cabai rawit - 4 buah', '1. Belah kepiting, buang bagian pembuangan yang berbentuk segitiga dan bagian insangnya, lalu cuci bersih. Goreng kepiting hingga matang. Angkat dan tiriskan.\r\n2. Panaskan mentega. Tumis bawang bombai hingga layu.\r\n3. Masukkan bumbu halus dan cabai merah besar. Tumis hingga harum.\r\n4. Masukkan saus sambal, saus tomat, kecap manis, saus tiram dan kecap inggris. Aduk rata.\r\n5. Tuang air dan bumbui dengan lada, garam dan gula. Masak hingga mendidih. Koreksi rasanya.\r\n6. Masukkan kepiting dan aduk r', 'Makanan Berat', 'kepiting saos padang.jpeg', '2024-04-19 05:29:42');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `username`, `password`, `time`) VALUES
(1, 'Aisy Karima', 'aisykarima', '12345', '2024-04-15 03:17:20'),
(2, 'Alya Maisa Hudanis', 'alyamaisa8', '12345', '2024-04-15 11:59:19'),
(3, 'Jessica Bunga Yuniartha', 'jessicae', '12345', '2024-04-15 11:59:19'),
(4, 'Beby Ayu Wulandari', 'beby_ayu', '12345', '2024-04-15 11:59:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabelresep`
--
ALTER TABLE `tabelresep`
  ADD PRIMARY KEY (`id_resep`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabelresep`
--
ALTER TABLE `tabelresep`
  MODIFY `id_resep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
