<?php
    require_once 'header_template.php';
    require_once 'footer_template.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail - Food Recipe</title>
    <link rel="stylesheet" href="beranda.css">
    <link href='https://fonts.googleapis.com/css?family=Nunito Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style type="text/css">
        /* detail content */
        .container {
            width : 900px;
            margin-left : auto;
            margin-right : auto;
        }
        .card-resep{
            margin-top: 15px;
            background-color: white;
            padding: 25px 65px 0px 40px;
            border-radius: 20px;
        }
        .card-resep img {
            width : 100%;
            height: 350px;
            object-fit: fill;
            border-style: solid;
            border-radius: 30px;
            padding: 10px 10px 10px 10px;
        }
        .card-body {
            padding : 15px;
        }
        .card-judul{
            margin-left: 50px;
        }
        .resep-kategori {
            display: flex;
            justify-content: center;
            align-items: center; /* Mengatur vertikal alignment */
            margin-bottom: 13px;
        }
        .isi-kategori {
            padding: 5px 10px;
            border-radius: 5px;
            background-color: #627254;
            margin-right: 15px;
            margin-left: 15px;
            color: #fff;
        }
        .resep-nama {
            text-align: center;
            font-size: 32px;
            font-weight: bold;
            margin-bottom: 8px;
        }
        .resep-deskripsi {
            text-align: center;
            font-size: 14px;
            color : #627254;
            margin-bottom: 20px;
        }
        .resep-alat {
            margin-bottom: 10px;
        }
        .resep-bahan {
            margin-bottom: 10px;
        }
        .resep-step {
            margin-bottom: 10px;
        }
    </style>

</head>
<body>
    <!--detail content-->
    <div class="container">
        <div class="card-resep">
            <img src="upload/nasi_goreng.jpg">

            <div class="card-body">
                <div class="card-judul">
                    <div class="resep-kategori">
                        <div class="isi-kategori">Makanan Berat</div>
                        <div class="isi-kategori">30 Menit</div>
                    </div>
                    <div class="resep-nama">Nasi Goreng Spesial</div>
                    <div class="resep-deskripsi">
                        <p>Nasi goreng yang lezat dengan bumbu spesial</p>
                        Siapa bilang nasi goreng harus biasa-biasa saja? Nasi goreng kami menghadirkan sensasi lezat yang bikin lidah bergoyang! 
                        Dibuat dengan cinta dan bumbu-bumbu pilihan, setiap suapan adalah perjalanan rasanya yang seru. 
                        Dari butir nasi yang renyah hingga potongan sayuran yang segar, semuanya dipadu dengan sentuhan rahasia kami yang membuat nasi goreng ini juara. 
                        Lezatnya nasi goreng gaul ini siap memuaskan selera tanpa harus repot memasak!</div>
                </div>
            
                    <div class="resep-alat">
                        <b>Alat :</b>
                        <ul>
                            <li>Wajan</li>
                            <li>Sendok</li>
                        </ul>
                    </div>
                    <div class="resep-bahan">
                        <b>Bahan :</b>
                        <ul>
                            <li>300g nasi putih</li>
                            <li>100g daging ayam/sapi/udang</li>
                            <li>2 butir telur</li>
                            <li>1 buah bawang merah</li>
                            <li>2 siung bawang putih</li>
                            <li>1 batang daun bawang</li>
                            <li>2 sdm kecap manis</li>
                            <li>1 sdm saus tiram</li>
                            <li>1 sdm minyak wijen</li>
                    </div>
                    <div class="resep-step">
                        <b>Langkah - Langkah :</b>
                        <ol>
                            <li>Tumis bawang merah dan bawang putih hingga harum.</li>
                            <li>Masukkan daging, telur, dan nasi, aduk rata.</li>
                            <li>Tambahkan kecap manis, saus tiram, dan minyak wijen, aduk hingga merata.</li>
                            <li>Taburkan daun bawang, aduk sebentar, lalu angkat.</li>
                        </ol>
                    </div>

            </div>
        </div>
    </div>

</body>
</html>