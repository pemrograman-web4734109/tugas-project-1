<?php
    session_start()
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home - Food Recipe</title>
    <link href='https://fonts.googleapis.com/css?family=Nunito Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style type="text/css">
        body, html {
    height: 100%;
    margin: 0;
    font-family: 'Nunito Sans', Arial, Helvetica, sans-serif;
    background-color: #F9F1F0;
  }
  *{
    padding: 0;
    margin: 0;
  }
  
  a {
    color: inherit;
    text-decoration: none;
  }
  /* navbar */
  .navbar {
  padding: 0.65rem 1rem;
  background-color: #627254;
  color: #fff;
  position: fixed;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 99;
  display: flex;
  }
  .navbar h1 {
  margin-left: 41%;
  font-size: 20px;
  line-height: 20px;
  }
  /* sidebar */
  .sidebar {
  position: fixed;
  width: 250px;
  top: 0;
  bottom: 0;
  background-color: #fff;
  padding-top: 40px;
  transition: all .5s;
  }
  .sidebar-hide {
  left: -250px;
  }
  .sidebar-show {
  left: 0;
  }
  .sidebar-body {
  padding: 15px;
  }
  .sidebar-body ul {
  list-style: none; /*menghilangkan titik list*/
  }
  .sidebar-body ul li a {
  width: 100%;
  display: inline-block;
  padding: 7px 15px;
  box-sizing: border-box;
  }
  /*saat kursor menyentuh border warna berubah*/
  .sidebar-body ul li a:hover {
  background-color: #627254;
  color: #fff;
  }
  /*memberikan border kecuali list terakhir*/
  .sidebar-body ul li:not(:last-child) {
  border-bottom: 1px solid #ccc;
  }
  
  .hero-image {
    border:1px solid;
    padding: 70px 15px 40px;
    background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("https://images.unsplash.com/photo-1563865436874-9aef32095fad?q=80&w=1887&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 100%;
  }
  
  .hero-text {
    text-align: center;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    color: white;
  }
  
  .hero-text button {
    border: none;
    outline: 0;
    display: inline-block;
    padding: 10px 25px;
    color: black;
    background-color: #ddd;
    text-align: center;
    cursor: pointer;
    border-radius: 5px;
  }
  
  .hero-text button:hover {
    background-color: #627254;
    color: white;
  }
  
    </style>
</head>
<body>
    <!--navbar-->
    <div class="navbar">
        <a href="#" id="btnBars">
            <i class="fa fa-bars"></i>
        </a>
    </div>

    <!--sidebar-->
    <div class="sidebar sidebar-hide">
        <div class="sidebar-body">
            <ul>
                <li><a href="beranda.php">Home</a></li>
                <li><a href="../dashboard/dashboard.php">Dashboard</a></li>
                <li><a href="../resep_add.php">Upload Resep</a></li>
                <li><a href="../tabelmakanan.php">Daftar Resep</a></li>
                <?php if(isset($_SESSION['uid'])): ?>
                    <li><a href="../logout.php">Logout</a></li>
                <?php else: ?>
                    <li><a href="../login.php">Login</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
    
    <!--hero image-->
    <div class="hero-image">
    <form action="../resep_add.php" method="post">
        <div class="hero-text">
            <h1>Online Food Recipe</h1>
            <p>You Can add Your Own Recipe!</p>
            <button type="submit" style="margin-top: 10px;">Upload Recipe</button>
        </div>  
    </form>
    </div>

    <script tipe="text/javascript">
        var btnBars = document.getElementById('btnBars')

        var sidebar = document.querySelector(".sidebar")
        
        btnBars.addEventListener('click', function(e){
            e.preventDefault();
            sidebar.classList.toggle('sidebar-show')
        })
    </script>
</body>
</html>
