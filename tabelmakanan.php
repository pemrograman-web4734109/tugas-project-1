 <?php
	require_once 'header_template.php';
	require_once 'footer_template.php';
	$query_select = 'SELECT * FROM resep.tabelResep';
	$run_query_select = mysqli_query($conn, $query_select);

	//cek jika ada parameter delete
	if(isset($_GET['delete'])){
		//proses delete data
		$query_delete = 'delete from resep.tabelResep where resep.tabelResep.id_resep = "'.$_GET['delete'].'"';
		$run_query_delete = mysqli_query($conn, $query_delete);

		if($run_query_delete){
			echo "<script>window.location = 'tabelmakanan.php'</script>";
		}else{
			echo "<script>alert('Data gagal dihapus')</script>";
		}
	}
?> 
<!DOCTYPE html>
<html lang=”en”>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<title>Daftar Resep - Food Recipe</title>
<style>
	.fa-solid:hover{
		color: #627254;
	}
	.fa-solid{
		color: #8ca16d;
	}
	.card{
		background-color: white;
		margin-top: 20px;
	}
	.column{
		float: left;
		padding-left: 20px;
	}
	.icon{
		width: 50px;
		margin: 0px 5px 0px 5px;
	}
</style>
</head>
<body>
<!-- Selamat datang <?php echo $_SESSION["user"]; ?>, <br> -->
	<div class="row">
	<div class="column">
		<h2>Daftar Resep</h2>
	</div>
	<div class="column"> <a href="resep_add.php" align="center" title="Posting resep baru"><i class="fa-solid fa-square-plus fa-2x"></i></i></a></div>
    <br>
	</div>
	<div class="card">
	<table border="1" cellpadding="25" cellspacing="0" width="">

	<tr>
		<th align="center">No.</th>
		<th>Nama Resep</th>
		<th>Deskripsi</th>
		<th>Alat</th>
		<th>Bahan</th>
		<th>Step</th>
		<th>Kategori</th>
        <th>Foto</th>
        <!-- <th>Waktu Upload</th> -->
		<th>Edit</th>
	</tr>

	<?php if(mysqli_num_rows($run_query_select) > 0){ ?>
	<?php $nomor = 1;?>
	<?php while($row = mysqli_fetch_array($run_query_select)){?>
	<tr>
		<td><?=$nomor++ ?></td>
		<td><?= $row['1']?></td>
		<td><?= $row['2']?></td>
		<td><?= $row['3']?></td>
		<td><?= $row['4']?></td>
		<td><?= $row['5']?></td>
		<td><?= $row['6']?></td>
		<td align="center"><img src="../upload/<?= $row['7']?>" width="90%" height="90%"></td>
		<!-- <td><?= $row['8']?></td> -->
		<td align="center" >
			<div class="icon">
		<a href="resep_edit.php?id=<?= $row["0"]; ?>" title="Ubah data" class=""><i class="fa-solid fa-square-pen"></i>&nbsp;|</a> 
		<a href="?delete=<?= $row["0"] ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus item ini?');" title="Hapus data"><i class="fa-solid fa-trash"></i></a>
			</div>
		</td>
	</tr>
	
	<?php }}else{ ?>

	<tr>
		<td colspan="10">Data tidak ditemukan</td>
	</tr>

	<?php } ?>
	</table>
	</div>

	

</body>
</html>


