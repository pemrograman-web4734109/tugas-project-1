<?php
    session_start();
    if(!isset($_SESSION['uid'])){
        header('location:../login.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Food Recipe</title>
    <link href='https://fonts.googleapis.com/css?family=Nunito Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style type="text/css">
        body, html {
          height: 100%;
          margin: 0;
          font-family: 'Nunito Sans', Arial, Helvetica, sans-serif;
          background-color: #F9F1F0;
        }
        *{
          padding: 0;
          margin: 0;
        }
        a {
          color: inherit;
          text-decoration: none;
        }
        /* navbar */
        .navbar {
          padding: 0.65rem 1rem;
          background-color: #627254;
          color: white;
          position: fixed;
          width: 100%;
          height: 42.5px;
          top: 0;
          left: 0;
          z-index: 99;
          display: flex;
        }
        .navbar h1 {
          margin-left: 42%;
          font-size: 20px;
          line-height: 20px;
          color: white;
        }
        /* sidebar */
        .sidebar {
          position: fixed;
          width: 250px;
          top: 0;
          bottom: 0;
          background-color: #fff;
          padding-top: 40px;
          transition: all .5s;
          z-index: 2;
        }
        .sidebar-hide {
          left: -250px;
        }
        .sidebar-show {
          left: 0;
        }
        .sidebar-body {
          padding: 15px;
        }
        .sidebar-body ul {
          list-style: none; /*menghilangkan titik list*/
        }
        .sidebar-body ul li a {
          width: 100%;
          display: inline-block;
          padding: 7px 15px;
          box-sizing: border-box;
        }
        /*saat kursor menyentuh border warna berubah*/
        .sidebar-body ul li a:hover {
          background-color: #627254;
          color: #fff;
        }
        /*memberikan border kecuali list terakhir*/
        .sidebar-body ul li:not(:last-child) {
          border-bottom: 1px solid #ccc;
        }
        /*content*/   
        .content {
          padding: 60px 0;
        } 
        .container {
          width: 960px;
          margin-left: auto;
          margin-right: auto;
        } 
        .page-title{
          margin-bottom: 10px;
        }
        .card {
          border: 1px solid #ccc;
          background-color: #fff;
          padding: 15px;
          border-radius: 5px;
        }
    </style>
</head>
<body>
    <!--navbar-->
    <div class="navbar">
        <a href="#" id="btnBars">
            <i class="fa fa-bars"></i>
        </a>
        <h1>Online Food Recipes</h1>
    </div>

    <!--sidebar-->
    <div class="sidebar sidebar-hide">
        <div class="sidebar-body">
            <ul>
                <li><a href="../beranda/beranda.php">Home</a></li>
                <li><a href="dashboard.php">Dashboard</a></li>
                <li><a href="../resep_add.php">Upload Resep</a></li>
                <li><a href="../tabelmakanan.php">Daftar Resep</a></li>
                <li><a href="../logout.php">Logout</a></li>
            </ul>
        </div>
    </div>
</body>
</html>