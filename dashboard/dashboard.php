<?php 
    require_once 'header_db.php';
    require_once 'footer_db.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Food Recipe</title>
    <link href='https://fonts.googleapis.com/css?family=Nunito Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha512-q3eWabyZPc1XTCmF+8/LuE1ozpg5xxn7iO89yfSOd5/oKvyqLngoNGsx8jq92Y8eXJ/IRxQbEC+FGSYxtk2oiw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="dashboard.css">
    <style type="text/css">
        body, html {
          height: auto;
          margin: 0;
          font-family: 'Nunito Sans', Arial, Helvetica, sans-serif;
          background-color: #FEFAF6;
        }
        *{
          padding: 0;
          margin: 0;
        }
        a {
          color: inherit;
          text-decoration: none;
        }
    </style>
</head>
<body>
  <div class="content">
    <div class="container" style="margin-top: 5px;">
        <div class="card" style="background-color: white">
            <h2>Hai <?= $_SESSION['uname'] ?>,</h2>
            <p>Selamat datang di website yang menyediakan berbagai macam resep makanan</p>
        </div>
    </div>
  </div>
  
  <div class="container-rekom">
      <div class="row text-center p-4">
        <div class="col-12">
          <h2 class="font-wigth-light">Rekomendasi</h2>
        </div>
      </div>
    </div>

    <div class="container text-center my-3">
      <div class="row mx-auto my-auto justify-content-center">
          <div class="col-md-4">
              <div class="card-hover">
                <div class="card-hover__content">
                    <h3 class="card-hover__title"> Nasi Goreng Jawa <span>Spesial</span></h3>
                    <p class="card-hover__text">Nasi goreng yang lezat dengan bumbu spesial</p>
                    <a href="../detail.php" class="card-hover__link">
                      <span>Lihat Resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://www.kitchensanctuary.com/wp-content/uploads/2020/07/Nasi-Goreng-square-FS-57.jpg" alt="">
                </div>
            </div>

            <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Pisang Goreng <span>Super</span> Crispy
                    </h3>
                    <p class="card-hover__text">Pisang goreng yang renyah diluar dan manis di dalam</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://1.bp.blogspot.com/-XVE7DSE1LGg/TZ7b0j1GVnI/AAAAAAAAACs/3doaa5Rl-ps/s1600/pisang_goreng.jpg" alt="">
              </div>   
          </div>
          
          <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Kepiting <span>Saos</span> Padang
                    </h3>
                    <p class="card-hover__text">Daging kepiting yang lembut dan manis sangat lezat dimasak dengan saos padang yang gurih</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://th.bing.com/th/id/OIP.uO9NKFOwKgVr8bRDS0eGeQHaJ4?w=675&h=900&rs=1&pid=ImgDetMain" alt="">
              </div>
          </div>

          <div class="col-md-4">
              <div class="card-hover">
                <div class="card-hover__content">
                    <h3 class="card-hover__title"> Tanghulu <br><span>Spesial</span></h3>
                    <p class="card-hover__text">Tanghulu dari buah segar yang membuat ketagihan</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat Resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="/upload/tanghulu-recipe-IG.jpg" alt="">
                </div>
            </div>

            <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Brownis <span>Kukus</span> Cokelat
                    </h3>
                    <p class="card-hover__text">Brownis Kukus Rasa Cokelat yang lembut dan empuk</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://th.bing.com/th/id/OIP.o3Wd3dK0MwKmW1TOEvV3YgHaE8?rs=1&pid=ImgDetMain" alt="">
              </div>   
          </div>
          
          <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Es <span>Buah</span> <br>Bakak
                    </h3>
                    <p class="card-hover__text">Buah-buahan yang segar dicampur dengan susu dan sirup sangat nikmat</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="/upload/es_buah.jpg" alt="">
              </div>
          </div>

          
          


      </div>
    </div>
</body>

